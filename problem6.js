// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  
// Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

let inventory = require('./data')

function findCars(carmake1, carmake2){
    let result = inventory.filter(car => car.car_make == carmake1 || car.car_make == carmake2)
    console.log(result);

}

module.exports = findCars;
